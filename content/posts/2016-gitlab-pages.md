Title: Démo Pélican
Date: 2020-04-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

Salut o/

Ce site est hébergé sur des pages Gitlab de Framasoft, et publié avec Pelican.

Source d'origine : <https://gitlab.com/pages/pelican>.

Plus d'info sur les GitLab Pages : <https://pages.gitlab.io>.
